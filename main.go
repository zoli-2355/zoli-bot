package main

import (
	"log"
	"os"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	botToken := os.Getenv("TELEGRAM_BOT_TOKEN")
	bot, err := tb.NewBot(tb.Settings{
		Token:  botToken,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	bot.Handle(tb.OnUserJoined, func (msg *tb.Message) {
		// log.Printf("delete user join message")
		bot.Delete(msg)
	})
	bot.Handle(tb.OnUserLeft, func (msg *tb.Message) {
		// log.Printf("delete user left message")
		bot.Delete(msg)
	})
	bot.Start()
}
