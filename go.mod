module zoli-bot

go 1.14

require (
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/tucnak/telebot.v2 v2.0.0-20200426184946-59629fe0483e
)
